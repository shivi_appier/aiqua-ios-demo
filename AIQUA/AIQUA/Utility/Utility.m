//
//  Utility.m
//  AIQUA
//
//  Created by Shiv.Raj on 9/8/18.
//  Copyright © 2018 Appier. All rights reserved.
//

#import "Utility.h"

@implementation Utility

static Utility *sharedInstance = nil;
static NSMutableArray *productsInCart = nil;
static NSString *remoteCategoryUrlStr = @"https://cdn.qgraph.io/aiqua-ec/aiqua-category.json";
static NSString *remoteProductsUrlStr = @"https://cdn.qgraph.io/aiqua-ec/aiqua-products.json";
static BOOL requestingRemoteData = NO;
static NSArray *remoteCategories = nil;
static NSDictionary *remoteProducts = nil;

+ (void)load {
    [Utility requestRemoteData];
}

+ (void)requestRemoteData {
    if (requestingRemoteData == NO) {
        requestingRemoteData = YES;
        dispatch_group_t group = dispatch_group_create();
        
        //config - Always looks up the original source
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        
        //enter
        dispatch_group_enter(group);
        NSURLSession *sessionCategories = [NSURLSession sessionWithConfiguration:configuration];
        [[sessionCategories dataTaskWithURL:[NSURL URLWithString:remoteCategoryUrlStr] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (error == nil) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                if ([httpResponse statusCode] == 200 && data) {
                    @try {
                        NSError *jsonError = nil;
                        NSArray *categories = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                        if (!jsonError && [categories isKindOfClass:[NSArray class]]) {
                            remoteCategories = categories;
                        }
                    } @catch (NSException *exception) {
                        NSLog(@"Utility: Error parsing jsondata of RemoteCategories: %@", exception);
                    }
                }
            }
            //leave
            dispatch_group_leave(group);
        }] resume];

        //enter
        dispatch_group_enter(group);
        NSURLSession *sessionProducts = [NSURLSession sessionWithConfiguration:configuration];
        [[sessionProducts dataTaskWithURL:[NSURL URLWithString:remoteProductsUrlStr] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (error == nil) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                if ([httpResponse statusCode] == 200 && data) {
                    @try {
                        NSError *jsonError = nil;
                        NSDictionary *products = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                        if (!jsonError && [products isKindOfClass:[NSDictionary class]]) {
                            remoteProducts = products;
                        }
                    } @catch (NSException *exception) {
                        NSLog(@"Utility: Error parsing jsondata of RemoteProducts: %@", exception);
                    }
                }
            }
            //leave
            dispatch_group_leave(group);
        }] resume];
        
        dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            requestingRemoteData = NO;
        });
    }
}

+ (NSArray *)country {
    static NSArray *data;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        data = @[
                    @{
                        @"name": @"select country",
                        @"appid": @""
                        },
                    @{
                        @"name": @"TAIWAN",
                        @"appid": @"a16661a99ea30aed9bcc",
                        },
                    @{
                        @"name": @"SINGAPORE",
                        @"appid": @"574d03bff99b7e9109ec"
                        },
                    @{
                        @"name": @"HONG KONG",
                        @"appid": @"b267e6b4691f149daa39"
                        },
                    @{
                        @"name": @"MALAYSIA",
                        @"appid": @"1bdf0a177263059f0bb0"
                        },
                    @{
                        @"name": @"INDONESIA",
                        @"appid": @"77719aa396cb7449e5bc"
                        },
                    @{
                        @"name": @"INDIA",
                        @"appid": @"8068d7be25327e083664"
                        },
                    @{
                        @"name": @"VIETNAM",
                        @"appid": @"8c0da543e7da03524584"
                        },
                    @{
                        @"name": @"THAILAND",
                        @"appid": @"af9eda0233474083adc9"
                        },
                    @{
                        @"name": @"JAPAN",
                        @"appid": @"74a51f3e69ed46e6e8e3"
                        },
                    @{
                        @"name": @"KOREA",
                        @"appid": @"c89f08fa249726e9fdb8"
                        }
                ];
    });
    return data;
}

// -------------------------------------------------
#pragma mark - Ecommerce Demo Data
// -------------------------------------------------
+ (NSArray *)category_local {
    static NSArray *categoryData;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        categoryData =
        @[
            @{
                @"_id": @"c0",
                @"name": @"Fashion",
                @"title": @"Shop\nNew Arrival in\nFashion",
                @"image": @"https://cdn.qgraph.io/img/aiqua-demo/ecomHomeScreen/ecomHomeScreen_CategoryA_cover.jpg",
                @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/ecomHomeScreen/ecomHomeScreen_CategoryA_thumbnail.jpg",
                @"deeplink": @"aiquademo://category/c0"
            },
            @{
                @"_id": @"c1",
                @"name": @"Shoes",
                @"title": @"Latest\nStyles in\nShoes",
                @"image": @"https://cdn.qgraph.io/img/aiqua-demo/ecomHomeScreen/ecomHomeScreen_CategoryB_cover.jpg",
                @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/ecomHomeScreen/ecomHomeScreen_CategoryB_thumbnail.jpg",
                @"deeplink": @"aiquademo://category/c1"
            },
            @{
                @"_id": @"c2",
                @"name": @"Electronics",
                @"title": @"Looking\nfor an\nUpgrade?",
                @"image": @"https://cdn.qgraph.io/img/aiqua-demo/ecomHomeScreen/ecomHomeScreen_CategoryC_cover.jpg",
                @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/ecomHomeScreen/ecomHomeScreen_CategoryC_thumbnail.jpg",
                @"deeplink": @"aiquademo://category/c2"
            },
            @{
                @"_id": @"c3",
                @"name": @"Pet Supplies",
                @"title": @"Only\nThe Best\nFor Your\nBest Friend",
                @"image": @"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/ecomHomeScreen_CategoryD_cover.jpg",
                @"thumbnail": @"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/ecomHomeScreen_CategoryD_thumbnail.jpg",
                @"deeplink": @"aiquademo://category/c3"
            }
        ];
    });
    return categoryData;
}

+ (NSDictionary *)products_local {
  static NSDictionary *data;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    data = @{
           @"c0":
           @[
           @{
             @"_id": @0,
             @"name": @"Generic Striped T-Shirt",
             @"category": @"Fashion",
             @"color": @"Black and White",
             @"description": @"Slim Fit",
             @"original_price": @"29.99",
             @"discounted_price": @"$ 19.99",
             @"discount": @"33%",
             @"rating": @"4.4",
             @"rating_count": @"5,112",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryA_product1.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryA_product1.jpg",
             @"deeplink": @"aiquademo://products/c0/0"
           },
           @{
             @"_id": @1,
             @"name": @"Generic Sweater",
             @"category": @"Fashion",
             @"color": @"Women Sweater",
             @"description": @"Half Sleeved",
             @"original_price": @"74.99",
             @"discounted_price": @"$ 49.99",
             @"discount": @"33%",
             @"rating": @"3.9",
             @"rating_count": @"8,434",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryA_product2.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryA_product2.jpg",
             @"deeplink": @"aiquademo://products/c0/1"
           },
           @{
             @"_id": @2,
             @"name": @"Generic Henley T-Shirt",
             @"category": @"Fashion",
             @"color": @"Blue and White",
             @"description": @"Regular Fit",
             @"original_price": @"29.99",
             @"discounted_price": @"$ 24.99",
             @"discount": @"17%",
             @"rating": @"4.1",
             @"rating_count": @"8,302",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryA_product3.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryA_product3.jpg",
             @"deeplink": @"aiquademo://products/c0/2"
           },
           @{
             @"_id": @3,
             @"name": @"Generic Checkered Shirt",
             @"category": @"Shoes",
             @"color": @"Blue, Black and White",
             @"description": @"Slim Fit, Full Sleeves",
             @"original_price": @"49.99",
             @"discounted_price": @"$ 39.99",
             @"discount": @"20%",
             @"rating": @"4.3",
             @"rating_count": @"1,978",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryA_product4.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryA_product4.jpg",
             @"deeplink": @"aiquademo://products/c0/3"
           }
           ],
           @"c1":
           @[
           @{
             @"_id": @0,
             @"name": @"Sports Shoes",
             @"category": @"Shoes",
             @"color": @"Grey Color",
             @"description": @"Foam Padding",
             @"original_price": @"129.99",
             @"discounted_price": @"$ 99.99",
             @"discount": @"23%",
             @"rating": @"4.5",
             @"rating_count": @"4,429",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryB_product1.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryB_product1.jpg",
             @"deeplink": @"aiquademo://products/c1/0"
           },
           @{
             @"_id": @1,
             @"name": @"Running Shoes",
             @"category": @"Shoes",
             @"color": @"Aqua Green",
             @"description": @"Foam Sole",
             @"original_price": @"399.99",
             @"discounted_price": @"$ 299.99",
             @"discount": @"25%",
             @"rating": @"4.0",
             @"rating_count": @"6,814",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryB_product2.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryB_product2.jpg",
             @"deeplink": @"aiquademo://products/c1/1"
           },
           @{
             @"_id": @2,
             @"name": @"Stilettos",
             @"category": @"Shoes",
             @"color": @"Blue and Gold",
             @"description": @"4 inch Heels",
             @"original_price": @"499.99",
             @"discounted_price": @"$ 249.99",
             @"discount": @"50%",
             @"rating": @"4.8",
             @"rating_count": @"1,446",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryB_product3.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryB_product3.jpg",
             @"deeplink": @"aiquademo://products/c1/2"
           },
           @{
             @"_id": @3,
             @"name": @"Sneakers",
             @"category": @"Shoes",
             @"color": @"Yellow Color",
             @"description": @"Lace-Up, Synthetic",
             @"original_price": @"249.99",
             @"discounted_price": @"$ 199.99",
             @"discount": @"20%",
             @"rating": @"3.9",
             @"rating_count": @"377",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryB_product4.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryB_product4.jpg",
             @"deeplink": @"aiquademo://products/c1/3"
           }
           ],
           @"c2":
           @[
           @{
             @"_id": @0,
             @"name": @"Generic Headphone",
             @"category": @"Electronics",
             @"color": @"Silver Color",
             @"description": @"Faux Leather Ear Pads",
             @"original_price": @"149.99",
             @"discounted_price": @"$ 99.99",
             @"discount": @"33%",
             @"rating": @"4.3",
             @"rating_count": @"9,256",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryC_product1.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryC_product1.jpg",
             @"deeplink": @"aiquademo://products/c2/0"
           },
           @{
             @"_id": @1,
             @"name": @"Generic Laptop",
             @"category": @"Electronics",
             @"color": @"Very Fast Processor",
             @"description": @"Included Optical Drive",
             @"original_price": @"1499.99",
             @"discounted_price": @"$ 1099.99",
             @"discount": @"27%",
             @"rating": @"3.5",
             @"rating_count": @"3,827",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryC_product2.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryC_product2.jpg",
             @"deeplink": @"aiquademo://products/c2/1"
           },
           @{
             @"_id": @2,
             @"name": @"Generic Printer",
             @"category": @"Electronics",
             @"color": @"A4 Size",
             @"description": @"Ink Jet Technology",
             @"original_price": @"249.99",
             @"discounted_price": @"$ 199.99",
             @"discount": @"20%",
             @"rating": @"4.5",
             @"rating_count": @"8,248",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryC_product3.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryC_product3.jpg",
             @"deeplink": @"aiquademo://products/c2/2"
           },
           @{
             @"_id": @3,
             @"name": @"Generic Speakers",
             @"category": @"Electronic",
             @"color": @"2.1 Channel",
             @"description": @"Powerful Bass",
             @"original_price": @"199.99",
             @"discounted_price": @"$ 149.99",
             @"discount": @"25%",
             @"rating": @"4.1",
             @"rating_count": @"9,987",
             @"image": @"https://cdn.qgraph.io/img/aiqua-demo/productScreen/productScreen_CategoryC_product4.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/aiqua-demo/productsListing/productListing_CategoryC_product4.jpg",
             @"deeplink": @"aiquademo://products/c2/3"
           }
           ],
           @"c3":
           @[
           @{
             @"_id": @0,
             @"name": @"Small Dog White Meat Chicken",
             @"category": @"Pet Supplies",
             @"color": @"7 lb. Bag",
             @"description": @"Bite-sized kibble design",
             @"original_price": @"17.99",
             @"discounted_price": @"$ 15.99",
             @"discount": @"11%",
             @"rating": @"4.3",
             @"rating_count": @"9,256",
             @"image": @"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/productScreen_CategoryD_product1.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/productListing_CategoryD_product1.jpg",
             @"deeplink": @"aiquademo://products/c3/0"
           },
           @{
             @"_id": @1,
             @"name": @"Dog Dental Chews Dog Treats",
             @"category": @"Pet Supplies",
             @"color": @"130-count pack",
             @"description": @"Natural Ingredients",
             @"original_price": @"29.99",
             @"discounted_price": @"$ 26.99",
             @"discount": @"10%",
             @"rating": @"3.5",
             @"rating_count": @"3,827",
             @"image": @"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/productScreen_CategoryD_product2.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/productListing_CategoryD_product2.jpg",
             @"deeplink": @"aiquademo://products/c3/1"
           },
           @{
             @"_id": @2,
             @"name": @"Bear Dog Toy",
             @"category": @"Pet Supplies",
             @"color": @"Soft and cuddly",
             @"description": @"Extra durability",
             @"original_price": @"14.99",
             @"discounted_price": @"$ 10.99",
             @"discount": @"33%",
             @"rating": @"4.5",
             @"rating_count": @"8,248",
             @"image": @"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/productScreen_CategoryD_product3.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/productListing_CategoryD_product3.jpg",
             @"deeplink": @"aiquademo://products/c3/2"
           },
           @{
             @"_id": @3,
             @"name": @"Premium Dog Bed",
             @"category": @"Pet Supplies",
             @"color": @"100% Polyester",
             @"description": @"Soft and Cozy",
             @"original_price": @"74.99",
             @"discounted_price": @"$ 49.99",
             @"discount": @"25%",
             @"rating": @"4.1",
             @"rating_count": @"9,987",
             @"image": @"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/productScreen_CategoryD_product4.jpg",
             @"thumbnail": @"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/productListing_CategoryD_product4.jpg",
             @"deeplink": @"aiquademo://products/c3/3"
           }
           ]
         };
     });
     return data;
}

+ (NSArray *)category {
    if (remoteCategories == nil || remoteProducts == nil) {
        [Utility requestRemoteData];
        return [Utility category_local];
    }
    return remoteCategories;
}

+ (NSDictionary *)products {
    if (remoteCategories == nil || remoteProducts == nil) {
        [Utility requestRemoteData];
        return [Utility products_local];
    }
    return remoteProducts;
}

// 5 types of push
+ (NSArray *)pushCreative {
    static NSArray *data;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        data = @[
                 @{
                     @"name": @"Carousel Push Notification",
                     @"type": @0,
                     @"event": @"carousel_push",
                     @"button": @"See Carousel",
                     @"detail": @"Send multiple images in a single notification to highlight promotions / products",
                     @"icon": @"thumbnail-carousel-push-ios"
                     },
                 @{
                     @"name": @"GIF in Push Notification",
                     @"type": @1,
                     @"event": @"gif_push",
                     @"button": @"See GIF Push",
                     @"detail": @"Send GIFs in your notification to grab users’ attention.",
                     @"icon": @"thumbnail-gif-in-push-ios"
                     },
                 @{
                     @"name": @"Video in Push Notification",
                     @"type": @2,
                     @"event": @"video_push",
                     @"button": @"See Video Push",
                     @"detail": @"Send videos in your push notifications",
                     @"icon": @"thumbnail-video-in-push-ios"
                     },
                 @{
                     @"name": @"Image in Push Notification",
                     @"type": @3,
                     @"event": @"image_push",
                     @"button": @"See Image Push",
                     @"detail": @"Catch users’ attention with animations a crowded notification center.",
                     @"icon": @"thumbnail-image-in-push-ios"
                     },
                 @{
                     @"name": @"Slider Push Notification",
                     @"type": @4,
                     @"event": @"slider_push",
                     @"button": @"See Slider",
                     @"detail": @"Send multiple big images in a notification, similar to the Carousel Push.",
                     @"icon": @"thumbnail-carousel-push-ios"
                     }
                ];
    });
    
    return data;
}

// 4 types of inapp
+ (NSArray *)inappCreative {
    static NSArray *data;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        data = @[
                 @{
                     @"name": @"Floating In-App Notifications",
                     @"type": @10,
                     @"event": @"floating-inapp",
                     @"button": @"See Floating In-App",
                     @"detail": @"Share a quick update without interrupting the user flow",
                     @"icon": @"thumbnail-floating-in-app-ios"
                     },
                 @{
                     @"name": @"Small In-App Notification",
                     @"type": @11,
                     @"event": @"small-inapp",
                     @"button": @"See Small In-App",
                     @"detail": @"An In-App format useful for sharing longer updates.",
                     @"icon": @"thumbnail-small-in-app-ios"
                     },
                 @{
                     @"name": @"Medium In-App Notification",
                     @"type": @12,
                     @"event": @"medium-inapp",
                     @"button": @"See Medium In-App",
                     @"detail": @"A rich In-App format with a cover image and optional message.",
                     @"icon": @"thumbnail-medium-in-app-ios"
                     },
                 @{
                     @"name": @"Full Screen In-App Notification",
                     @"type": @13,
                     @"event": @"full-screen-inapp",
                     @"button": @"See Full Screen In-App",
                     @"detail": @"Show a full screen image with a call to action to engage users.",
                     @"icon": @"thumbnail-full-screen-in-app-ios"
                     }
            ];
    });
    
    return data;
}

+ (void)addProductToCart:(Product *)product {
    if (productsInCart == nil) {
        productsInCart = [NSMutableArray new];
    }
    [productsInCart addObject:product];
}

+ (NSArray *)productsFromCart {
    return productsInCart;
}

+ (void)cleanCart {
    if (productsInCart) {
        [productsInCart removeAllObjects];
    }
}

+ (void)cleanProductFromCart:(Product *)product {
    if (productsInCart) {
        for (int i = 0; i < productsInCart.count; i++) {
            if ([productsInCart objectAtIndex:i] == product) {
                [productsInCart removeObjectAtIndex:i];
                return;
            }
        }
      }
}
@end

