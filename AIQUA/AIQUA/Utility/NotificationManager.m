//
//  NotificationManager.m
//  AIQUA
//
//  Created by Shiv.Raj on 15/8/18.
//  Copyright © 2018 Appier. All rights reserved.
//

#import "NotificationManager.h"
#import <UserNotifications/UserNotifications.h>
#import "QGSdk.h"
#import "Category.h"
#import "Product.h"

#import "ProductDetailViewController.h"
#import "CategoryCollectionViewController.h"
#import "DemoEcomViewController.h"
#import "DemoSplashViewController.h"
#import "HomeViewController.h"


@interface NotificationManager () <UNUserNotificationCenterDelegate>

@end

@implementation NotificationManager

static NotificationManager *sharedInstance = nil;

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NotificationManager alloc] init];
    });
    return sharedInstance;
}

//----------------------------------------------
#pragma mark - Push Permission & SDK Initialise
//----------------------------------------------

- (void)initializeSDKWithAppId:(NSString *)appid {
    QGSdk *qgsdk = [QGSdk getSharedInstance];
#ifdef DEBUG
    [qgsdk onStart:appid withAppGroup:APP_GROUP setDevProfile:true];
#else
    [qgsdk onStart:appid withAppGroup:APP_GROUP setDevProfile:false];
#endif
}

- (void)requestPushPermission {
    QGSdk *qgsdk = [QGSdk getSharedInstance];
    
    if (@available(iOS 10.0, *)) {
        UNAuthorizationOptions options = (UNAuthorizationOptions) (UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionCarPlay);
//        if (@available(iOS 12.0, *)) {
//            if (![qgsdk getShowPushPrompt]) {
//                //add provisional for silent push in notification center without push prompt
//                options = options | UNAuthorizationOptionProvisional;
//            }
//        }
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        
        [center requestAuthorizationWithOptions:options completionHandler:^(BOOL granted, NSError *error){
            NSLog(@"GRANTED: %i, Error: %@", granted, error);
        }];
    } else {
        // Fallback on earlier versions - iOS 8 & 9
        UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeSound |
        UIUserNotificationTypeBadge;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    
    // save inapp campaign
    // if (![[NSUserDefaults standardUserDefaults] boolForKey:@"local_inapp_saved"]) {
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"inapp" ofType:@"json"]];
    NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    @try {
        [qgsdk performSelector:@selector(saveInAppCampagins:) withObject:payload];
    }
    @catch (NSException *exception) {
        NSLog(@"Caught exception %@ - %@", exception, @"InApp features may be not supported now");
    }
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"local_inapp_saved"];
}

//------------------------------------------
#pragma mark - UNNotificationCenterDelegate
//------------------------------------------

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler  API_AVAILABLE(ios(10.0)){
    [[QGSdk getSharedInstance] userNotificationCenter:center willPresentNotification:notification];
    UNNotificationPresentationOptions option = UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert;
    
    completionHandler(option);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler  API_AVAILABLE(ios(10.0)){
    [[QGSdk getSharedInstance] userNotificationCenter:center didReceiveNotificationResponse:response];
    
    completionHandler();
}


//------------------------------------
#pragma mark - Handle Notifications
//------------------------------------
/*
 * Handle only push notifications
 * For In-App, just log event with the specific event name
 */

- (void)createNotificationWithType:(Type)type {
    QGSdk *qg = [QGSdk getSharedInstance];
    switch (type) {
        case kTypePushImage:
            [self createImagePush];
            [qg logEvent:PUSH_TRIGGERED withParameters:@{NOTIFICATION_TEMPLATE: PUSH_IMAGE}];
            break;
        case kTypePushGIF:
            [self createGIFPush];
            [qg logEvent:PUSH_TRIGGERED withParameters:@{NOTIFICATION_TEMPLATE: PUSH_GIF}];
            break;
        case kTypePushVideo:
            [self createVideoPush];
            [qg logEvent:PUSH_TRIGGERED withParameters:@{NOTIFICATION_TEMPLATE: PUSH_VIDEO}];
            break;
        case kTypePushCarousel:
            [self createCarouselPush];
            [qg logEvent:PUSH_TRIGGERED withParameters:@{NOTIFICATION_TEMPLATE: PUSH_CAROUSEL}];
            break;
        case kTypePushSlider:
            [self createSliderPush];
            [qg logEvent:PUSH_TRIGGERED withParameters:@{NOTIFICATION_TEMPLATE: PUSH_SLIDER}];
            break;
        case kTypeInAppFloating:
            [qg logEvent:INAPP_TRIGGERED withParameters:@{INAPP_TEMPLATE: INAPP_FLOATING}];
            break;
        case kTypeInAppSmall:
            [qg logEvent:INAPP_TRIGGERED withParameters:@{INAPP_TEMPLATE: INAPP_SMALL}];
            break;
        case kTypeInAppMedium:
            [qg logEvent:INAPP_TRIGGERED withParameters:@{INAPP_TEMPLATE: INAPP_MEDIUM}];
            break;
        case kTypeInAppLarge:
            [qg logEvent:INAPP_TRIGGERED withParameters:@{INAPP_TEMPLATE: INAPP_FULL_SCREEN}];
            break;
        default:
            break;
    }
}

#pragma mark - Image Push

- (void)createImagePush {
    NSString * title = @"Push Notification with Image";
    NSString * body = @"You can attach any .JPEG or .PNG upto 10 MB in size as an attachment to your push notification.";
    NSString * subtitle = @"Optional Subtitle";
    
    if (@available(iOS 10.0, *)) {
        NSDictionary * userInfo = @{@"source":@"QG", @"qg":@"101", @"nid": @"101000", @"qgPush":@{@"type":@"image", @"url":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/image-push.jpg"}};
        
        NSURL *square = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"image-push" ofType:@"jpg"]];
        UNNotificationAttachment *attachment = [UNNotificationAttachment attachmentWithIdentifier:@"image-push" URL:square options:nil error:nil];
        NSArray <UNNotificationAttachment *> *attachments = @[attachment];
        
        [self createLocalNotificationWithTitle:title subtitle:subtitle body:body userInfo:userInfo type:kTypePushVideo attachments:attachments];
    } else {
        // Fallback on earlier versions
        NSLog(@"Media attachment not allowed in ios less than 10.0");
        [self createLocalNotificationWithTitle:title withBody:body];
    }
}

#pragma mark - GIF Push

- (void)createGIFPush {
    NSString * title = @"Push Notification with GIF";
    NSString * body = @"A cool & fun format to engage your users. Add a GIF file as an attachment to your push notification.";
    NSString * subtitle = @"Optional Subtitle";
    
    if (@available(iOS 10.0, *)) {
        NSDictionary * userInfo = @{@"source":@"QG", @"qg":@"102", @"nid": @"102000", @"qgPush":@{@"type":@"gif", @"url":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/gif-push.gif"}};
        
        NSURL *square = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"gif-push" ofType:@"gif"]];
        UNNotificationAttachment *attachment = [UNNotificationAttachment attachmentWithIdentifier:@"gif-push" URL:square options:nil error:nil];
        NSArray <UNNotificationAttachment *> *attachments = @[attachment];
        
        [self createLocalNotificationWithTitle:title subtitle:subtitle body:body userInfo:userInfo type:kTypePushVideo attachments:attachments];
    } else {
        // Fallback on earlier versions
        NSLog(@"Media attachment not allowed in ios less than 10.0");
        [self createLocalNotificationWithTitle:title withBody:body];
    }
}

#pragma mark - Video Push

- (void)createVideoPush {
    NSString * title = @"Push Notification with Video";
    NSString * body = @"A format for the mobile first era. Include MPEG/MPEG2Video/MPEG4/AVI (upto 50 MB) file as a attachment.";
    NSString * subtitle = @"Optional Subtitle";
    
    if (@available(iOS 10.0, *)) {
        NSDictionary * userInfo = @{@"source":@"QG", @"qg":@"103", @"nid": @"103000", @"qgPush":@{@"type":@"video", @"url":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/video-push.mp4"}};
        
        NSURL *square = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"video-push" ofType:@"mp4"]];
        UNNotificationAttachment *attachment = [UNNotificationAttachment attachmentWithIdentifier:@"video-push" URL:square options:nil error:nil];
        
        NSArray <UNNotificationAttachment *> *attachments = @[attachment];
        
        [self createLocalNotificationWithTitle:title subtitle:subtitle body:body userInfo:userInfo type:kTypePushVideo attachments:attachments];
    } else {
        // Fallback on earlier versions
        NSLog(@"Media attachment not allowed in ios less than 10.0");
        [self createLocalNotificationWithTitle:title withBody:body];
    }
}

#pragma mark - Carousel Push

- (void)createCarouselPush {
    NSString * title = @"Carousel Push Notification";
    NSString * subtitle = @"Carousel in Linear Style";
    NSString * body = @"Send upto 10 images in a single notification to highlight different promotions or products";
    
    if (@available(iOS 10.0, *)) {
        NSArray *data = @[@{@"title":@"Holidays in Thailand", @"body":@"", @"imageUrl":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/carousel_1.jpg", @"deepLink":@"aiquademo://"}, @{@"title":@"Travel to Japan", @"body":@"", @"imageUrl":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/carousel_2.jpg", @"deepLink":@"aiquademo://"}, @{@"title":@"Discover Experiences in Japan", @"body":@"", @"imageUrl":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/carousel_3.jpg", @"deepLink":@"aiquademo://"}, @{@"title":@"Japan Food & Drink", @"body":@"", @"imageUrl":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/carousel_4.jpg", @"deepLink":@"aiquademo://"}, @{@"title":@"Must Eats in Thailand", @"body":@"", @"imageUrl":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/carousel_5.jpg", @"deepLink":@"aiquademo://"}];
        NSString *carouselType = @"Linear";
        NSString *aspect = @"fit";
        
        NSDictionary * userInfo = @{@"source":@"QG", @"qg":@"104", @"nid": @"104000", @"qgPush":@{@"type":@"carousel", @"custom":@{@"carouselType":carouselType, @"aspect":aspect, @"data":data}}};
        
        NSURL *carousel1 = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"carousel_1" ofType:@"jpg"]];
        NSURL *carousel2 = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"carousel_2" ofType:@"jpg"]];
        NSURL *carousel3 = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"carousel_3" ofType:@"jpg"]];
        NSURL *carousel4 = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"carousel_4" ofType:@"jpg"]];
        NSURL *carousel5 = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"carousel_5" ofType:@"jpg"]];
        
        UNNotificationAttachment *attachment1 = [UNNotificationAttachment attachmentWithIdentifier:@"carousel_1" URL:carousel1 options:nil error:nil];
        UNNotificationAttachment *attachment2 = [UNNotificationAttachment attachmentWithIdentifier:@"carousel_2" URL:carousel2 options:nil error:nil];
        UNNotificationAttachment *attachment3 = [UNNotificationAttachment attachmentWithIdentifier:@"carousel_3" URL:carousel3 options:nil error:nil];
        UNNotificationAttachment *attachment4 = [UNNotificationAttachment attachmentWithIdentifier:@"carousel_4" URL:carousel4 options:nil error:nil];
        UNNotificationAttachment *attachment5 = [UNNotificationAttachment attachmentWithIdentifier:@"carousel_5" URL:carousel5 options:nil error:nil];
        NSArray <UNNotificationAttachment *> *attachments = @[attachment1, attachment2, attachment3, attachment4, attachment5];
        
        [self createLocalNotificationWithTitle:title subtitle:subtitle body:body userInfo:userInfo type:kTypePushCarousel attachments:attachments];
    } else {
        // Fallback on earlier versions
        NSLog(@"Media attachment not allowed in ios less than 10.0");
        [self createLocalNotificationWithTitle:title withBody:body];
    }
}

#pragma mark - Slider Push

- (void)createSliderPush {
    NSString * title = @"Slider Push Notification";
    NSString * body = @"Send upto 10 images in a single notification to highlight different promotions or products. ";
    NSString * subtitle = @"Better suited for landscape images";
    
    if (@available(iOS 10.0, *)) {
        NSString *carouselType = @"Slider";
        NSString *aspect = @"fill";
        
        NSArray *data = @[@{@"title":@"Summer Sale!", @"body":@"", @"imageUrl":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/slider_1.jpg", @"deepLink":@"aiquademo://"}, @{@"title":@"Upto 50% Off", @"body":@"", @"imageUrl":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/slider_2.jpg", @"deepLink":@"aiquademo://"}, @{@"title":@"Last few days, hurry!", @"body":@"", @"imageUrl":@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/slider_3.jpg", @"deepLink":@"aiquademo://"}];
        
        NSDictionary * userInfo = @{@"source":@"QG", @"qg":@"105", @"nid": @"105000", @"qgPush":@{@"type":@"carousel", @"custom":@{@"carouselType":carouselType, @"aspect":aspect, @"data":data}}};
        
        NSURL *slider1 = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"slider_1" ofType:@"jpg"]];
        NSURL *slider2 = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"slider_2" ofType:@"jpg"]];
        NSURL *slider3 = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"slider_3" ofType:@"jpg"]];
        
        UNNotificationAttachment *attachment1 = [UNNotificationAttachment attachmentWithIdentifier:@"slider_1" URL:slider1 options:nil error:nil];
        UNNotificationAttachment *attachment2 = [UNNotificationAttachment attachmentWithIdentifier:@"slider_2" URL:slider2 options:nil error:nil];
        UNNotificationAttachment *attachment3 = [UNNotificationAttachment attachmentWithIdentifier:@"slider_3" URL:slider3 options:nil error:nil];
        NSArray <UNNotificationAttachment *> *attachments = @[attachment1, attachment2, attachment3];
        
        [self createLocalNotificationWithTitle:title subtitle:subtitle body:body userInfo:userInfo type:kTypePushSlider attachments:attachments];
    } else {
        // Fallback on earlier versions
        NSLog(@"Media attachment not allowed in ios less than 10.0");
        [self createLocalNotificationWithTitle:title withBody:body];
    }
    
}

// create notification - iOS 10 and above
- (void)createLocalNotificationWithTitle:(NSString *)title
                                subtitle:(NSString *)subtitle
                                    body:(NSString *)body
                                userInfo:(NSDictionary *)userInfo
                                    type:(Type)type
                             attachments:(NSArray <UNNotificationAttachment *> *)attachments API_AVAILABLE(ios(10.0)) {
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.title = title;
    content.subtitle = subtitle;
    content.body = body;
    content.userInfo = userInfo;
    content.sound = [UNNotificationSound defaultSound];
    content.attachments = attachments;
    
    if ((type == kTypePushCarousel || type == kTypePushSlider) && [self hasRichPushSupport]) {
        content.categoryIdentifier = @"QGCAROUSEL";
    }
    
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:false];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"local-Push" content:content trigger:trigger];
    
    [center addNotificationRequest:request withCompletionHandler:^(NSError *error){
        if (!error) {
            NSLog(@"Local Push Scheduled");
        }
    }];
}

// create notification - iOS 9 and below
- (void)createLocalNotificationWithTitle:(NSString *)title
                                withBody:(NSString *)body {
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertTitle = title;
    notification.alertBody = body;
    notification.userInfo = @{@"source":@"QG", @"qg":@"1001", @"qgPush":@{@"type":@"basic"}};
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

- (BOOL)hasRichPushSupport {
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP];
    BOOL supported = [[defaults valueForKey:@"qg_rich_push_notification_supported_status"] boolValue];
    return supported;
}

//-------------------------------
#pragma mark - Handle DeepLink
//-------------------------------

- (void)handleDeeplinkWithURL:(NSURL *)url withNavigation:(UINavigationController *)nav {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    if ([url.host isEqualToString:@"products"]) {
        NSString *product_id = url.lastPathComponent;
        NSString *category_id = url.pathComponents[1];
        
        Category *category = [self getCategoryWithId:category_id];
        if (!category) {
            return;
        }
        CategoryCollectionViewController *cvc = [storyboard instantiateViewControllerWithIdentifier:@"CategoryVC"];
        cvc.title = category.name;
        cvc.category = category;
        cvc.categoryProducts = [Utility products][category._id];
        
        Product *product = [self getProductWithId:product_id withCategory:category_id];
        if (!product) {
            return;
        }
        ProductDetailViewController *pvc = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
        pvc.product = product;
        
        [self pushToViewWithNav:nav withViews:@[cvc, pvc]];
    } else if ([url.host isEqualToString:@"category"]) {
        Category *category = [self getCategoryWithId:url.lastPathComponent];
        if (!category) {
            return;
        }
        CategoryCollectionViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"CategoryVC"];
        vc.title = category.name;
        vc.category = category;
        vc.categoryProducts = [Utility products][category._id];
        
        [self pushToViewWithNav:nav withViews:@[vc]];
    }
}

- (void)pushToViewWithNav:(UINavigationController *)nav withViews:(NSArray <UIViewController *> *)views {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    HomeViewController *home = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    DemoSplashViewController *demoSplash = [storyboard instantiateViewControllerWithIdentifier:@"DemoSplash"];
    DemoEcomViewController *demoEcom = [storyboard instantiateViewControllerWithIdentifier:@"DemoEcomVC"];
    
    [nav setNavigationBarHidden:NO animated:NO];
    nav.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [nav setViewControllers:[@[home, demoSplash, demoEcom] arrayByAddingObjectsFromArray:views] animated:YES];
}

- (Product *)getProductWithId:(NSString *)_id withCategory:(NSString *)category_id {
    NSArray *products = [Utility products][category_id];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"_id == %d", _id.integerValue];
    NSArray *filteredData = [products filteredArrayUsingPredicate:filter];
    if (filteredData.count == 0) {
        return nil;
    }
    Product *product = [[Product alloc] initWithJSONData:filteredData[0]];
    
    return product;
}

- (Category *)getCategoryWithId:(NSString *)_id {
    NSArray *categories = [Utility category];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"_id == %@", _id];
    NSArray *filteredData = [categories filteredArrayUsingPredicate:filter];
    if (filteredData.count == 0) {
        return nil;
    }
    Category *category = [[Category alloc] initWithJSONData:filteredData[0]];
    
    // mark last viewed category
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults boolForKey:CATEGORY_VIEWED_STATUS]) {
        [defaults setBool:true forKey:CATEGORY_VIEWED_STATUS];
    }
    [defaults setValue:category.image forKey:LAST_VIEWED_CATEGORY_IMAGE];
    [defaults setValue:category.title forKey:LAST_VIEWED_CATEGORY_TITLE];
    [defaults setValue:category.name forKey:LAST_VIEWED_CATEGORY_NAME];
    
    return category;
}

@end
