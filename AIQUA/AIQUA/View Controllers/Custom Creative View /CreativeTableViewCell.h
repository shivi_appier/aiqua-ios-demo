//
//  CreativeTableViewCell.h
//  AIQUA
//
//  Created by Shiv.Raj on 15/8/18.
//  Copyright © 2018 Appier. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Creative;
@protocol CreativeCellProtocol;

@interface CreativeTableViewCell : UITableViewCell

- (void)initWithData:(Creative *)creative;

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIButton *typeButton;
@property (weak, nonatomic) IBOutlet UILabel *typeName;
@property (weak, nonatomic) IBOutlet UILabel *typeDetail;

@property (nonatomic) Creative *creative;

@property (nonatomic, assign) id <CreativeCellProtocol> delegate;

@end

@protocol CreativeCellProtocol <NSObject>

- (void)creativeCellSelected:(Creative *)creative;

@end
