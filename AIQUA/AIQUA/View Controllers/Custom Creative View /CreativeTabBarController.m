//
//  CreativeTabBarController.m
//  AIQUA
//
//  Created by Shiv.Raj on 14/8/18.
//  Copyright © 2018 Appier. All rights reserved.
//

#import "CreativeTabBarController.h"
#import "Utility.h"
#import "CreativeTableViewCell.h"
#import "Creative.h"
#import "NotificationManager.h"
#import "QGSdk.h"

@interface CreativeTabBarController () <UITableViewDelegate, UITableViewDataSource, CreativeCellProtocol>

@property (weak, nonatomic) IBOutlet UIButton *pushTab;
@property (weak, nonatomic) IBOutlet UIButton *inappTab;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSArray * currentCreative;

@end

@implementation CreativeTabBarController

static NSString * Identifier = @"NotificationCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView registerNib:[UINib nibWithNibName:@"CreativeTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:Identifier];
    
    [self initialSetup];
    
    // page viewed event
    [[QGSdk getSharedInstance] logEvent:PAGE_VIEWED withParameters:@{PAGE_NAME: TEMPLATE_LISTING}];
}

#pragma mark - <UITableViewDelegate>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.currentCreative.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CreativeTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    Creative * creative = [[Creative alloc] initWithJSONData:self.currentCreative[indexPath.row]];
    cell.creative = creative;
    cell.delegate = self;
    
    [cell initWithData:creative];
    
    return cell;
}

#pragma mark <CreativeCellProtocol>

- (void)creativeCellSelected:(Creative *)creative {
    [[QGSdk getSharedInstance] logEvent:creative.event];
    [[NotificationManager sharedInstance] createNotificationWithType:creative.type];
}

- (IBAction)tabClicked:(UIButton *)sender {
    if (sender.tag == 0) {
        // Push Clicked
        [self updateViewForTab:true];
    } else {
        // InApp Clicked
        [self updateViewForTab:false];
    }
}

- (void)updateViewForTab:(BOOL)pushTab {
    self.pushTab.selected = pushTab;
    self.inappTab.selected = !pushTab;
    
    // change background color
    // selected - BLACK_OPACITY_20; default - CLEAR_COLOR
    self.pushTab.backgroundColor = pushTab ? BLACK_OPACITY_20 : CLEAR_COLOR;
    self.inappTab.backgroundColor = !pushTab ? BLACK_OPACITY_20 : CLEAR_COLOR;
    
    self.currentCreative = pushTab ? [Utility pushCreative] : [Utility inappCreative];
    
    [self.tableView reloadData];
}

- (void)initialSetup {
    [self updateViewForTab:true];
}

@end
