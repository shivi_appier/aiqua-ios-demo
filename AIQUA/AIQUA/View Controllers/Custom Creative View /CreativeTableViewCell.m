//
//  CreativeTableViewCell.m
//  AIQUA
//
//  Created by Shiv.Raj on 15/8/18.
//  Copyright © 2018 Appier. All rights reserved.
//

#import "CreativeTableViewCell.h"
#import "Creative.h"

@implementation CreativeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.typeButton.titleLabel.textAlignment = NSTextAlignmentCenter;
}

- (void)initWithData:(Creative *)creative {
    self.typeName.text = creative.name;
    self.typeDetail.text = creative.detail;
    [self.typeButton setTitle:creative.button forState:UIControlStateNormal];
    self.iconImageView.image = [UIImage imageNamed:creative.icon];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (IBAction)typeButtonClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(creativeCellSelected:)]) {
        [self.delegate creativeCellSelected:self.creative];
    }
}

@end
