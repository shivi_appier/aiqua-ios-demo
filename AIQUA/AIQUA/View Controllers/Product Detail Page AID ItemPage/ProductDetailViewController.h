//
//  ProductDetailViewController.h
//  AIQUA
//
//  Created by Shiv.Raj on 13/8/18.
//  Copyright © 2018 Appier. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Product;
@interface ProductDetailViewController : UIViewController

@property (nonatomic) Product *product;

@end
