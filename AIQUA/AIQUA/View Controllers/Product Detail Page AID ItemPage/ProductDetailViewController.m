//
//  ProductDetailViewController.m
//  AIQUA
//
//  Created by Shiv.Raj on 13/8/18.
//  Copyright © 2018 Appier. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "Product.h"
#import "UIImageView+WebCache.h"
#import "Utility.h"
#import "QGSdk.h"
#import "AIDManager.h"
#import "CartViewController.h"
#import "SettingsPopOverViewController.h"

@interface ProductDetailViewController ()<UIPopoverPresentationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UILabel *productColor;
@property (weak, nonatomic) IBOutlet UILabel *productDescription;

@property (weak, nonatomic) IBOutlet UIView *ratingView;

@property (weak, nonatomic) IBOutlet UILabel *productRating;
@property (weak, nonatomic) IBOutlet UILabel *productRatingCount;

@property (weak, nonatomic) IBOutlet UILabel *discoutedPrice;
@property (weak, nonatomic) IBOutlet UILabel *originalPrice;
@property (weak, nonatomic) IBOutlet UILabel *discount;
@property (weak, nonatomic) IBOutlet UIButton *wishlistButton;
@property (weak, nonatomic) IBOutlet UIButton *cartButton;

@end

@implementation ProductDetailViewController

@synthesize product;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.imageView.layer.masksToBounds = YES;
    
    self.navigationItem.title = product.name;
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:product.image] placeholderImage:[UIImage imageNamed:@"preview-image"]];
    
    self.productName.text = product.name;
    self.productColor.text = [NSString stringWithFormat:@"• %@", product.color];
    self.productDescription.text = [NSString stringWithFormat:@"• %@", product.description];
    self.productRating.text = product.rating;
    self.productRatingCount.text = [product.ratingsCount stringByAppendingString:@" ratings"];
    self.discoutedPrice.text = product.discountedPrice;
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:product.originalPrice];
    [string addAttribute:NSStrikethroughStyleAttributeName value:@1 range:NSMakeRange(0, string.length)];
    self.originalPrice.attributedText = string;
    
    self.discount.text = [NSString stringWithFormat:@"%@ off", product.discount];
    
    [self sendEventWithName:PRODUCT_VIEWED];
    
    self.title = @"Item/Product Page";
    
    //Right Bar Item
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 44)];
    UIButton *btnCart = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [btnCart setImageEdgeInsets:UIEdgeInsetsMake(6, 12, 6, 0)];
    [btnCart setImage:[UIImage imageNamed:@"cart_icon"] forState:UIControlStateNormal];
    [btnCart addTarget:self action:@selector(onRightTopBarCartClicked:) forControlEvents:UIControlEventTouchUpInside];
    [customView addSubview:btnCart];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:customView];
    self.navigationItem.rightBarButtonItems = @[rightItem, [self createSettingBtn]];
}

- (void)onRightTopBarCartClicked:(id)sender {
    NSLog(@"onRightTopBarCartClicked");
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain
                                                                            target:nil action:nil];
    [self.navigationController pushViewController:[CartViewController new] animated:YES];
}

- (UIBarButtonItem *)createSettingBtn {
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 44)];
    UIButton *btnCart = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [btnCart setImageEdgeInsets:UIEdgeInsetsMake(6, 12, 6, 0)];
    [btnCart setImage:[UIImage imageNamed:@"setting_icon"] forState:UIControlStateNormal];
    [btnCart addTarget:self action:@selector(onSetting:) forControlEvents:UIControlEventTouchUpInside];
    [customView addSubview:btnCart];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:customView];
    return rightItem;
}

- (void)onSetting:(id)sender {
    NSLog(@"setting");
    UIView *view = (UIView *)sender;
    SettingsPopOverViewController *settingPopOver = [[SettingsPopOverViewController alloc] initWithCleanSessionOn:NO];
    settingPopOver.preferredContentSize = [settingPopOver customPreferredContentSize];
    settingPopOver.modalPresentationStyle = UIModalPresentationPopover;
    settingPopOver.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown | UIPopoverArrowDirectionUp;
    settingPopOver.popoverPresentationController.delegate = self;
    settingPopOver.popoverPresentationController.sourceView = view;
    settingPopOver.popoverPresentationController.sourceRect = [view frame];
    [self presentViewController:settingPopOver animated:YES completion:nil];
}

-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    NSLog(@"%s", __func__);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[AIDManager instance] initPage:self PageType:AIDPageType_Item Scrollable:nil IsLogin:NO
                          ItemPrice:[NSNumber numberWithInt:100] CartPrice:[NSNumber numberWithInt:1000]];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[AIDManager instance] deinitPage];
}

- (IBAction)addedToWishList:(UIButton *)sender {
    if (sender.selected) {
        [sender setSelected:false];
    } else {
        [sender setSelected:true];
        [self sendEventWithName:ADDED_TO_WISHLIST];
    }
}

- (IBAction)addedToCart:(UIButton *)sender {
    if (sender.selected) {
        [sender setSelected:false];
        
        //remove
        [Utility cleanProductFromCart:product];
    } else {
        [sender setSelected:true];
        [self sendEventWithName:ADDED_TO_CART];
        
        //add
        [Utility addProductToCart:product];
    }
}

- (void)sendEventWithName:(NSString *)eventName {
    [[QGSdk getSharedInstance] logEvent:eventName withParameters:@{
                                                                     PRODUCT_NAME: product.name,
                                                                     PRODUCT_PRICE: product.discountedPrice,
                                                                     PRODUCT_RATING: product.rating,
                                                                     PRODUCT_CATEGORY: product.category,
                                                                     PRODUCT_IMAGE_URL: product.image,
                                                                     PRODUCT_DEEPLINK: product.deeplink
                                                                     }];
}

@end
