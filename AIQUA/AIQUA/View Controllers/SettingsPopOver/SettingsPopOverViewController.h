//
//  SettingsPopOverViewController.h
//  AIQUA
//
//  Created by chunta on 2020/6/2.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingsPopOverViewController : UIViewController
- (instancetype)initWithCleanSessionOn:(BOOL)cleanSessionEnabled;
- (CGSize)customPreferredContentSize;
@end

NS_ASSUME_NONNULL_END
