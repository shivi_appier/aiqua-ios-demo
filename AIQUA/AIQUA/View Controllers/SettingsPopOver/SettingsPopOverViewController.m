//
//  SettingsPopOverViewController.m
//  AIQUA
//
//  Created by chunta on 2020/6/2.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "SettingsPopOverViewController.h"
#import "AIDManager.h"

@interface SettingsPopOverViewController ()
@property(nonatomic, strong) IBOutlet UIView *containerViewInCleanSessionMode;
@property(nonatomic, strong) IBOutlet UIView *containerViewInSendCampaignMode;
@property(nonatomic, strong) IBOutlet UISwitch *controlGroupSwitch;
@property(nonatomic, assign) BOOL cleanSessionMode;
@end

@implementation SettingsPopOverViewController
- (instancetype)initWithCleanSessionOn:(BOOL)cleanSessionEnabled {
    self = [super init];
    self.cleanSessionMode = cleanSessionEnabled;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"cleanSessionMode %d", self.cleanSessionMode);
    self.containerViewInCleanSessionMode.hidden = !self.cleanSessionMode;
    self.containerViewInSendCampaignMode.hidden = self.cleanSessionMode;
}

- (CGSize)customPreferredContentSize {
    if (self.cleanSessionMode) {
        return CGSizeMake(200, 60 * 2);
    }
    return CGSizeMake(200, 60);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wundeclared-selector"
    [self.controlGroupSwitch setOn:NO];
    if ([[AIDManager instance] performSelector:@selector(qaModeIsControlGroup)]) {
        [self.controlGroupSwitch setOn:YES];
    }
    #pragma clang diagnostic pop
}

- (IBAction)onCleanSession:(id)sender {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wundeclared-selector"
    [[AIDManager instance] performSelector:@selector(qaModeCleanAllStorage)];
    #pragma clang diagnostic pop
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onSendCampaign:(id)sender {
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setObject:@"aideal-mobile-qa" forKey:@"apikey"];
    [dic setObject:@(18203) forKey:@"element_id"];
    [dic setObject:@(17523) forKey:@"campaign_id"];
    [dic setObject:@(9310) forKey:@"incentive_id"];
    [dic setObject:@"coupon_code" forKey:@"incentive_type"];
    [dic setObject:@"QACode" forKey:@"coupon_code"];
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wundeclared-selector"
    [[AIDManager instance] performSelector:@selector(onAIDSocketReceivedCampaign:) withObject:dic];
    #pragma clang diagnostic pop
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onValueChanged:(id)sender {
    UISwitch *theSwitch = (UISwitch *)sender;
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wundeclared-selector"
    [[AIDManager instance] performSelector:@selector(qaModeToggleGroup:) withObject:@(theSwitch.isOn)];
    #pragma clang diagnostic pop
}
@end
