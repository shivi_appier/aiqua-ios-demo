//
//  CartViewController.m
//  AIQUA
//
//  Created by chunta on 2020/4/6.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "CartViewController.h"
#import "CartFormCheckoutViewController.h"
#import "CartTableViewCell.h"
#import "AIDManager.h"
#import "Utility.h"
#import "Product.h"

#define CartViewCellIdentifier @"CartTableViewCell"

@interface CartViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property(nonatomic)IBOutlet UITableView *tableView;
@property(nonatomic)IBOutlet UILabel *totalPriceLabel;
@property(nonatomic)IBOutlet UITextField *couponInputField;
@property(nonatomic)IBOutlet UIButton *proceedBtn;
@end

@implementation CartViewController
@synthesize tableView;
@synthesize totalPriceLabel;
@synthesize couponInputField;
@synthesize proceedBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Shopping-cart Page";
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    couponInputField.delegate = self;
    
    [tableView registerNib:[UINib nibWithNibName:CartViewCellIdentifier bundle:nil] forCellReuseIdentifier:CartViewCellIdentifier];
    
    proceedBtn.enabled = ([Utility productsFromCart].count > 0) ? YES : NO;
    proceedBtn.alpha = (proceedBtn.enabled == NO) ? 0.4 : 1;
    
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = UITableViewAutomaticDimension;
    
    //Total
    float total = 0;
    for (int i = 0; i < [Utility productsFromCart].count; i++) {
        Product *product = [[Utility productsFromCart] objectAtIndex:i];
        NSString *discountPrice = [product.discountedPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
        total += [discountPrice floatValue];
    }
    totalPriceLabel.text = [NSString stringWithFormat:@"%0.2f", total];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[AIDManager instance] initPage:self PageType:AIDPageType_Cart Scrollable:nil IsLogin:YES
                             ItemPrice:nil CartPrice:[NSNumber numberWithInt:900]];    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear: animated];
    [[AIDManager instance] deinitPage];
}

- (IBAction)onProceedToOrder:(id)sender {
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:[CartFormCheckoutViewController new] animated:YES];
}

#pragma mark - UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [Utility productsFromCart].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CartViewCellIdentifier];
    Product *product = [[Utility productsFromCart] objectAtIndex:indexPath.row];
    if (product) {
        cell.name.text = product.name;
        cell.price.text = product.discountedPrice;
    }
    return cell;
}

#pragma mark - UITextInputDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

@end
