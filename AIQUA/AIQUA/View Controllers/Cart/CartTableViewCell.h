//
//  CartTableViewCell.h
//  AIQUA
//
//  Created by chunta on 2020/4/8.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CartTableViewCell : UITableViewCell
@property(nonatomic, strong)IBOutlet UILabel *name;
@property(nonatomic, strong)IBOutlet UILabel *price;
@end

NS_ASSUME_NONNULL_END
