//
//  PersonalizationDemoViewController.m
//  AIQUA
//
//  Created by Shiv.Raj on 10/8/18.
//  Copyright © 2018 Appier. All rights reserved.
//

#import "DemoSplashViewController.h"
#import "DemoEcomViewController.h"
#import "SettingsPopOverViewController.h"

@interface DemoSplashViewController ()<UIPopoverPresentationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *startView;
@property (weak, nonatomic) IBOutlet UIButton *startDemoButton;

@end

@implementation DemoSplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializeLayout];
    
    // Setting right bar button
    self.navigationItem.rightBarButtonItem = [self createSettingBtn];
}

- (void)viewDidAppear:(BOOL)animated {
    // add shadow effect
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_startView.bounds];
    _startView.layer.masksToBounds = NO;
    _startView.layer.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4].CGColor;
    _startView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    _startView.layer.shadowOpacity = 0.5f;
    _startView.layer.shadowRadius = 8;
    _startView.layer.shadowPath = shadowPath.CGPath;
}

- (IBAction)startButtonClicked:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    DemoEcomViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"DemoEcomVC"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:vc animated:true];
}

- (UIBarButtonItem *)createSettingBtn {
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 44)];
    UIButton *btnCart = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [btnCart setImageEdgeInsets:UIEdgeInsetsMake(6, 12, 6, 0)];
    [btnCart setImage:[UIImage imageNamed:@"setting_icon"] forState:UIControlStateNormal];
    [btnCart addTarget:self action:@selector(onSetting:) forControlEvents:UIControlEventTouchUpInside];
    [customView addSubview:btnCart];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:customView];
    return rightItem;
}

- (void)onSetting:(id)sender {
    NSLog(@"setting");
    UIView *view = (UIView *)sender;
    SettingsPopOverViewController *settingPopOver = [[SettingsPopOverViewController alloc] initWithCleanSessionOn:YES];
    settingPopOver.preferredContentSize = [settingPopOver customPreferredContentSize];
    settingPopOver.modalPresentationStyle = UIModalPresentationPopover;
    settingPopOver.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown | UIPopoverArrowDirectionUp;
    settingPopOver.popoverPresentationController.delegate = self;
    settingPopOver.popoverPresentationController.sourceView = view;
    settingPopOver.popoverPresentationController.sourceRect = [view frame];
    [self presentViewController:settingPopOver animated:YES completion:nil];
}

-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    NSLog(@"%s", __func__);
}

- (void)initializeLayout {
    [self changeButtonStyle:self.startDemoButton];
}

- (void)changeButtonStyle:(UIButton *)button {
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor colorWithRed:36/255.0 green:172/255.0 blue:255/255.0 alpha:0.7].CGColor;
    button.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
}

@end
