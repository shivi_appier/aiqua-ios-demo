//
//  CartFormCheckoutViewController.m
//  AIQUA
//
//  Created by chunta on 2020/4/6.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "CartFormCheckoutViewController.h"
#import "ConversionViewController.h"
#import "AIDManager.h"
#import "Utility.h"
#import "Product.h"
@interface CartFormCheckoutViewController ()
@property(nonatomic)IBOutlet UILabel *totalPriceLabel;
@end

@implementation CartFormCheckoutViewController
@synthesize totalPriceLabel;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Shopping-cart Form";
    
    //Total
    float total = 0;
    for (int i = 0; i < [Utility productsFromCart].count; i++) {
        Product *product = [[Utility productsFromCart] objectAtIndex:i];
        NSString *discountPrice = [product.discountedPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
        total += [discountPrice floatValue];
    }
    totalPriceLabel.text = [NSString stringWithFormat:@"%0.2f", total];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[AIDManager instance] initPage:self PageType:AIDPageType_CartForm Scrollable:nil IsLogin:YES
                             ItemPrice:nil CartPrice:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[AIDManager instance] deinitPage];
}

- (IBAction)onConversion:(id)sender {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:[ConversionViewController new]  animated:YES];
}
@end
