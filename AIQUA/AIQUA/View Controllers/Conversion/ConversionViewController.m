//
//  ConversionViewController.m
//  AIQUA
//
//  Created by chunta on 2020/4/6.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "ConversionViewController.h"
#import "DemoEcomViewController.h"
#import "AIDManager.h"
#import "Utility.h"
@interface ConversionViewController ()
@property(nonatomic, strong)IBOutlet UIButton *shoppingBtn;
@property(nonatomic, strong)IBOutlet NSLayoutConstraint *botMarginShoppingBtn;
@end

@implementation ConversionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Conversion/Checkout Page";
    
    self.navigationItem.hidesBackButton = YES;
    self.botMarginShoppingBtn.constant = -100;
    
    //Clean all products from cart
    [Utility cleanCart];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:0.4 delay:0.1 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
        self.botMarginShoppingBtn.constant = 50;
        [self.view layoutIfNeeded]; } completion:nil];
 
    
    [[AIDManager instance] initPage:self PageType:AIDPageType_Conversion Scrollable:nil IsLogin:NO
                             ItemPrice:nil CartPrice:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[AIDManager instance] deinitPage];
    NSLog(@"conversion deinit ... ");
}

- (IBAction)onTop:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:NO];
}
@end

