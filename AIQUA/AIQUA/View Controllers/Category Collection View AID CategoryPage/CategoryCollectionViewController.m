//
//  CategoryCollectionViewController.m
//  AIQUA
//
//  Created by Shiv.Raj on 13/8/18.
//  Copyright © 2018 Appier. All rights reserved.
//
#import "CategoryCollectionViewController.h"
#import "CategoryCollectionViewCell.h"
#import "CartFormCheckoutViewController.h"
#import "ProductDetailViewController.h"
#import "CartViewController.h"
#import "Product.h"
#import "Utility.h"
#import "QGSdk.h"
#import "AIDManager.h"
#import "Category.h"
#import "SettingsPopOverViewController.h"

@interface CategoryCollectionViewController ()<UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UIPopoverPresentationControllerDelegate>
@property (nonatomic)NSMutableArray *categorySearchProducts;
@property (nonatomic)UISearchBar *searchBar;
@property (nonatomic)NSInteger topMargin;
@end

@implementation CategoryCollectionViewController

@synthesize categoryProducts;
@synthesize categorySearchProducts;
@synthesize category;
@synthesize searchBar;
@synthesize topMargin;
@synthesize useSearch;

static NSString * const reuseIdentifier = @"CategoryCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (useSearch) {
        [[QGSdk getSharedInstance] logEvent:CATEGORY_VIEWED];
    } else {
        [[QGSdk getSharedInstance] logEvent:CATEGORY_VIEWED withParameters:@{CATEGORY_NAME: category.name, CATEGORY_DEEPLINK: category.deeplink}];
    }
    
    //Right Bar Item
    if (useSearch == NO) {
        UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 44)];
        UIButton *btnCart = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [btnCart setImageEdgeInsets:UIEdgeInsetsMake(6, 12, 6, 0)];
        [btnCart setImage:[UIImage imageNamed:@"cart_icon"] forState:UIControlStateNormal];
        [btnCart addTarget:self action:@selector(onRightTopBarCartClicked:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:btnCart];
        //btnCart.layer.borderWidth = 1;
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:customView];
        self.navigationItem.rightBarButtonItems = @[rightItem, [self createSettingBtn]];
    }
    
    self.title = @"Category Page";
    
    if (self.useSearch) {
          
        //Title
        self.title = @"Search Page";
        
        //Top safearea including status bar and iPhoneX margin
        UIViewController *topViewCtl = UIApplication.sharedApplication.keyWindow.rootViewController;
        if (@available(iOS 11.0, *)) {
            topMargin = topViewCtl.view.safeAreaInsets.top;
        } else {
            // Fallback on earlier versions
            topMargin = topViewCtl.topLayoutGuide.length;
        }
        
        //NavigationBar height
        if (self.navigationController) {
            NSLog(@"%f", self.navigationController.navigationBar.frame.size.height);
            topMargin += self.navigationController.navigationBar.frame.size.height; // Fix NavigationBar Height
        }
    
        if (searchBar == nil) {
            //Fix search bar height, according to Apple UI Guidance
            CGRect searchBarRect = CGRectMake(0, topMargin, self.view.bounds.size.width, 44);
            searchBar = [[UISearchBar alloc] initWithFrame:searchBarRect];
            searchBar.delegate = self;
            searchBar.showsCancelButton = YES;
            [self.view addSubview:searchBar];
        }
        self.collectionView.frame = CGRectMake(0, topMargin + searchBar.frame.size.height,
                                               self.collectionView.frame.size.width,
                                               self.collectionView.frame.size.height - topMargin - searchBar.frame.size.height);
    }
}

- (void)onRightTopBarCartClicked:(id)sender {
    NSLog(@"onRightTopBarCartClicked");
   self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain
                                                                           target:nil action:nil];
    [self.navigationController pushViewController:[CartViewController new] animated:YES];
}

- (UIBarButtonItem *)createSettingBtn {
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 44)];
    UIButton *btnCart = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [btnCart setImageEdgeInsets:UIEdgeInsetsMake(6, 12, 6, 0)];
    [btnCart setImage:[UIImage imageNamed:@"setting_icon"] forState:UIControlStateNormal];
    [btnCart addTarget:self action:@selector(onSetting:) forControlEvents:UIControlEventTouchUpInside];
    [customView addSubview:btnCart];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:customView];
    return rightItem;
}

- (void)onSetting:(id)sender {
    NSLog(@"setting");
    UIView *view = (UIView *)sender;
    SettingsPopOverViewController *settingPopOver = [[SettingsPopOverViewController alloc] initWithCleanSessionOn:NO];
    settingPopOver.preferredContentSize = [settingPopOver customPreferredContentSize];
    settingPopOver.modalPresentationStyle = UIModalPresentationPopover;
    settingPopOver.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown | UIPopoverArrowDirectionUp;
    settingPopOver.popoverPresentationController.delegate = self;
    settingPopOver.popoverPresentationController.sourceView = view;
    settingPopOver.popoverPresentationController.sourceRect = [view frame];
    [self presentViewController:settingPopOver animated:YES completion:nil];
}

-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    NSLog(@"%s", __func__);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[AIDManager instance] initPage:self PageType:(useSearch) ? AIDPageType_Search : AIDPageType_Category Scrollable:self.collectionView IsLogin:YES ItemPrice:nil CartPrice:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[AIDManager instance] deinitPage];
}

#pragma mark <UISearchBarDelegate>

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    NSLog(@"DidEnd --- Search text:%@", searchBar.text);
    [self searchByString:searchBar.text];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    NSLog(@"Click --- Search text:%@", searchBar.text);
    [self searchByString:searchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    NSLog(@"Cancel --- Search text:%@", searchBar.text);
    [self searchByString:searchBar.text];
}

#pragma mark Search
- (void)searchByString:(NSString *)searchStr {
    NSString *theSearchStr = [searchStr lowercaseString];
    if (searchStr && searchStr.length) {
        //Reset
        categorySearchProducts = [NSMutableArray new];
          
        // Filter out by search string
        for (int i = 0; i < categoryProducts.count; i++) {
            NSDictionary *oneProduct = [categoryProducts objectAtIndex:i];
            if ([oneProduct objectForKey:@"name"]) {
                if ([[[oneProduct objectForKey:@"name"] lowercaseString] containsString:theSearchStr]) {
                    [categorySearchProducts addObject:oneProduct];
                    continue;;
                }
            }
        };
    } else {
        //Reset
        categorySearchProducts = nil;
    }
    [self.collectionView reloadData];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (categorySearchProducts) {
        return categorySearchProducts.count;
    }
    return categoryProducts.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    if (categorySearchProducts) {
        
        Product *product = [[Product alloc] initWithJSONData:categorySearchProducts[indexPath.row]];
         [cell initWithData: product];
        return cell;
    }
    Product *product = [[Product alloc] initWithJSONData:categoryProducts[indexPath.row]];
    if (indexPath.row == 0) {
        product.name = @"Buy-Direct";
    }
    [cell initWithData: product];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    Product *product = nil;
    if (categorySearchProducts) {
        product = [[Product alloc] initWithJSONData:categorySearchProducts[indexPath.row]];
    } else {
        product = [[Product alloc] initWithJSONData:categoryProducts[indexPath.row]];
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    if (indexPath.row == 0) {
        CartFormCheckoutViewController *cartVC = [[CartFormCheckoutViewController alloc] init];
        [self.navigationController pushViewController:cartVC animated:YES];
        return;
    }
    ProductDetailViewController *vc = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
    vc.product = product;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect rect = self.view.bounds;
    
//    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    CGFloat height = 260;
    CGFloat width = (rect.size.width - 36)/2;
    
    return CGSizeMake(width, height);
}

@end
