//
//  NotificationService.h
//  NotificationService
//
//  Created by qgraph on 14/09/16.
//  Copyright © 2016 QuantumGraph. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
